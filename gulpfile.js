var gulp = require('gulp');
var less = require('gulp-less');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
var plumber = require('gulp-plumber');
var connect = require('gulp-connect');

gulp.task('less', function() {
  return gulp.src(['./css/*.less', '!./css/nocompile/**/*.less'])
    .pipe(plumber())
	.pipe(sourcemaps.init())
  	.pipe(autoprefixer({
		browsers: ['last 2 versions'],
	}))
    .pipe(less())
    .pipe(gulp.dest('./css'))
    .pipe(connect.reload());
});

gulp.task('html', function() {
    gulp.src('./**/*.html')
        .pipe(connect.reload());
});

gulp.task('connect', function() {
    connect.server();
});

gulp.task('watch', function() {
    gulp.watch(['./css/**/*.less'], ['less']);
    gulp.watch(['./**/*.html'], ['html']);
});


gulp.task('default',['connect','watch']);
