# [Portfolio for Nick VanMeter](http://nickvanmeter.com)

Created with [HTML5 Boilerplate](http://html5boilerplate.com)

## I am a Front End Developer.

With over five years of professional experience, I build cutting-edge experiences in HTML5, CSS3 and Javascript for both small and large business websites and web applications.

## My launguages
* HTML5
* CSS3 + LESS/Sass
* Javascript + ES2015
* PHP
* Sarcasm

## My frameworks
* React
* Knockout
* jQuery
* Drupal

## My Other Skills
* Adobe Illustrator
* Adobe Photoshop
* Git
* Agile/Scrum Proficient
* Bilingual in Developer & Designer

## Get in touch
* [Twitter](http://www.twitter.com/nickjvm)
* [Facebook](http://www.facebook.com/nick.vanmeter)
* [Instagram](https://www.instagram.com/nick.vanmeter)
* [LinkedIn](https://www.linkedin.com/in/nickvanmeter)